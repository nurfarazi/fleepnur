//
//  ViewController.m
//  test
//
//  Created by nur on 3/7/15.
//  Copyright (c) 2015 inventivegame. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)ac:(id)sender {
    
    UIButton *btnAnimate = (UIButton *)sender;
    btnAnimate.transform = CGAffineTransformIdentity;
    
    [UIView animateWithDuration:1.4 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        btnAnimate.transform = CGAffineTransformMakeScale(1.3f, 0.5f);
        btnAnimate.transform = CGAffineTransformMakeTranslation(20, 50);
        
    } completion:^(BOOL finished){
        
        [btnAnimate setFrame:(CGRectMake(10, 10, 10, 19))];
        
    }];
}
@end
